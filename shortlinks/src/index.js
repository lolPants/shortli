// Package Dependencies
const Koa = require('koa')
const Router = require('koa-router')

// Redis DB
const Redis = require('ioredis')
const redis = new Redis('redis')

// Koa Server
const app = new Koa()
const router = new Router()

// Standard Middleware
app.use(require('koa-morgan')('combined'))
app.use(require('@koa/cors')())

router.get('/:key', async ctx => {
  const lookup = await redis.get(ctx.params.key)
  if (lookup) {
    ctx.status = 301
    ctx.redirect(lookup)
    ctx.body = `Redirecting to ${lookup}...`
  } else {
    ctx.throw(404)
  }
})

app
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(3000)
