# 🔗 Shortli ![](https://gitlab.com/lolPants/shortli/badges/master/build.svg)
_A simple, deployable URL shortening service built on Node.js, Redis and Docker._

## ❓ Usage
### 📋 Prerequisites
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### ☁ Docker Registry
GitLab hosts all the Docker Images on it's own Container Registry for this Repository.  
More info is available [here](https://gitlab.com/nerd3-servers/nerd3bot-cogs/container_registry).

### 💾 Images
If you want to update the image, do `docker-compose pull`.  
Docker will pull a fresh pre-built image automatically on first run if you don't have one saved locally.

If you have cloned the repo locally then you can also use `docker-compose build` to build all images from source.

### 🔧 Environment Variables
Copy `example.env` to `shortli.env` and fill in the variables. It's self documented so see in the file for more info.  

### 🚀 Deployment
Start the service in detached mode with automatic restarting with `docker-compose up -d`

The service runs on two ports:
- `51850` for the frontend and API server.
- `51851` for the redirection server.

It is recommended that you use a reverse proxy such as NGINX to serve these.

#### Frontend / API
The React-based frontend and API server run all in one for easy deployment.

#### Redirection Server
A simple server able to handle many requests per second that redirects if a short URL exists for its key.  
It's on a different port allowing you to serve your frontend on one url and the shortlinks on a shorter domain name. (`twitter.com` and `t.co` is a good example of this.)

## ✅ Licenses
- Link Emoji used from [FxEmoji](https://github.com/mozilla/fxemoji) ([Creative Commons Attribution 4.0 International](https://github.com/mozilla/fxemoji/blob/gh-pages/LICENSE.md))
- [Redis](https://redis.io/) ([Three Clause BSD License](https://opensource.org/licenses/BSD-3-Clause))
- [Express](https://github.com/expressjs/express) ([MIT License](https://github.com/expressjs/express/blob/master/LICENSE))
- [Koa](https://github.com/koajs/koa) ([MIT License](https://github.com/koajs/koa/blob/master/LICENSE))
- [ioredis](https://github.com/luin/ioredis) ([MIT License](https://github.com/luin/ioredis/blob/master/LICENSE))

See the `package.json` files for a full list of dependencies and their licenses. Shortli is noncommercial and itself licensed under the [ISC License](https://gitlab.com/lolPants/shortli/blob/master/LICENSE)
