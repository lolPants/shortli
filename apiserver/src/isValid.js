// Package Dependencies
const { parse } = require('url')
const nslookup = require('nslookup')
const { isWebUri } = require('valid-url')

// Environment Variables
const { SHORTLI_DISABLE_DNS } = process.env

/**
 * Check a that a URL resolves through DNS
 * @param {string} url Input URL to Resolve
 * @returns {Promise.<string[]>}
 */
const checkDNS = url => new Promise((resolve, reject) => {
  let { hostname } = parse(url)
  nslookup(hostname)
    .server('8.8.8.8')
    .type('a')
    .timeout(10 * 1000)
    .end((err, address) => {
      if (err) reject(err)
      else resolve(address)
    })
})

/**
 * Checks the validity of a URL.
 *
 * Ensures a URL is valid according to RFC 3986 URI Specification
 * and that it resolves to an IP address.
 * @param {string} url Input URL to Check
 * @returns {Promise.<boolean>}
 */
const isValid = async url => {
  if (!isWebUri(url)) return false

  if (SHORTLI_DISABLE_DNS) return true
  try {
    let dns = await checkDNS(url)
    if (dns.length > 0) return true
    else return false
  } catch (err) {
    return false
  }
}

module.exports = { isValid }
