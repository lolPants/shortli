// Package Dependencies
const Redis = require('ioredis')
const express = require('express')
const morgan = require('morgan')
const RateLimit = require('express-rate-limit')
const cors = require('cors')
const { json } = require('body-parser')
const { generate } = require('randomstring')

// HTTP Proxy
const { createProxyServer } = require('http-proxy')
const proxy = createProxyServer()

// Local Dependencies
const { isValid } = require('./isValid')

// Setup Express App
const app = express()
const api = express()
app.enable('trust proxy')
app.use(morgan('combined'))
api.use(cors())
api.use(json())
let ratelimit = new RateLimit({ windowMs: 10 * 60 * 1000, max: 100, delayMs: 0 })

// Redis DB
const redis = new Redis('redis')

// Environment Variables
const { SHORTLI_HOST } = process.env

api.put('/new', ratelimit, async (req, res) => {
  try {
    // Handle missing or invalid body
    if (!req.body) return res.status(400).send('Missing JSON Body')
    if (!req.body.url) return res.status(400).send('Missing URL key in JSON Body')

    // Ensure the URL is valid
    let { url } = req.body
    if (!await isValid(url)) return res.status(403).send('URL is Invalid')

    // Stop URLs from this service being shortened
    if (SHORTLI_HOST && url.includes(SHORTLI_HOST)) res.status(401).send('You cannot shorten that URL')

    // Check if the URL already has an associated key
    let key = await redis.hget('keys', url)
    if (key) {
      let short = SHORTLI_HOST ? `${SHORTLI_HOST}/${key}` : key
      return res.send({ short, url })
    }

    // Define character set for generating a random key
    let abc = 'abcdefghijklmnopqrstuvwxyz'
    let num = '0123456789'
    let charset = `${abc + abc.toUpperCase() + num}-_`

    // Generate a unique short ID
    let id = false
    while (!id) {
      let randomStr = generate({
        length: 6,
        charset,
      })

      if (await redis.get(randomStr)) continue // eslint-disable-line
      else id = randomStr
    }

    // Define the returned short URL
    let short = SHORTLI_HOST ? `${SHORTLI_HOST}/${id}` : id

    // Save in DB and send
    await redis.set(id, url)
    await redis.hset('keys', url, id)
    return res.send({ short, url })
  } catch (err) {
    res.status(500).send(err)
  }
})

api.get('/meta', (req, res) => {
  let title = process.env.SHORTLI_TITLE || 'Shortli'
  let subtitle = process.env.SHORTLI_SUBTITLE || 'A simple, deployable URL shortening service built on Node.js, Redis and Docker.'

  return res.send({ title, subtitle })
})

// Use routes
app.use('/api/v1.0', api)

// Proxy main stuff through
app.all('/*', (req, res) => {
  proxy.web(req, res, { target: 'http://frontend:80' })
})

// Listen for HTTP
app.listen(3000)
