import React, { Component } from 'react'
import PropTypes from 'prop-types'

import c from './constants'

class Form extends Component {
  constructor (props) {
    super(props)

    this.state = {
      input: '',
      buttonStyle: '',
      tooltip: '',
      tooltipStyle: '',
      tooltipClickable: false,
    }
  }

  handleInput (event) {
    this.setState({
      input: event.target.value,
      buttonStyle: '',
    })
  }

  async handleSubmit () {
    this.setState({ buttonStyle: 'is-loading' })
    let url = this.state.input

    if (url === window.location.href) return this.errorState(c.ERR_CANNOT_SHORTEN)

    try {
      let shorten = await fetch('/api/v1.0/new', {
        method: 'PUT',
        body: JSON.stringify({ url }),
        headers: new Headers({ 'Content-Type': 'application/json' }),
      })

      switch (shorten.status) {
        case 400:
          this.errorState(c.ERR_URL_MISSING)
          break
        case 401:
          this.errorState(c.ERR_CANNOT_SHORTEN)
          break
        case 403:
          this.errorState(c.ERR_URL_INVALID)
          break
        case 404:
          this.errorState(c.ERR_API_UNREACHABLE)
          break
        case 200:
          this.successState(shorten)
          break
        default:
          this.errorState(c.ERR_UNKNOWN)
      }
    } catch (err) {
      this.errorState(c.ERR_OFFLINE)
    }
  }

  errorState (text) {
    this.setState({
      tooltip: text,
      buttonStyle: 'is-danger',
      tooltipStyle: 'is-danger',
      tooltipClickable: false,
    })
  }

  async successState (shorten) {
    let { short } = await shorten.json()

    this.setState({
      input: '',
      buttonStyle: 'is-success',
      tooltip: short,
      tooltipStyle: '',
      tooltipClickable: true,
    })

    this.inputBox.focus()
  }

  render () {
    return (
      <div>
        <div style={{
          display: 'flex',
          justifyContent: 'center',
        }}>
          <div className='field has-addons is-fullwidth'>
            <div className='control' style={{ maxWidth: '550px', width: '56vw' }}>
              <input
                className='input'
                type='text'
                placeholder={ c.INPUT_PLACEHOLDER }
                value={this.state.input}
                onChange={e => { this.handleInput(e) }}
                onKeyDown={ e => { if (e.key === 'Enter') this.handleSubmit(e) }}
                ref={input => { this.inputBox = input }}
              />
            </div>
            <div className='control'>
              <a className={ `button is-info ${this.state.buttonStyle}` } onClick={() => { this.handleSubmit() }}>
                { c.BUTTON_SHORTEN }
              </a>
            </div>
          </div>
        </div>
        <ToolTip
          text={ this.state.tooltip }
          style={ this.state.tooltipStyle }
          clickable={ this.state.tooltipClickable }
        />
      </div>
    )
  }
}

const ToolTip = props => {
  let isBlank = props.text === ''
  let hiddenStyle = isBlank ? 'is-invis' : ''
  let text = isBlank ? '-' : props.text

  if (!props.clickable) {
    return <p className={ `help ${hiddenStyle} ${props.style}` }>{ text }</p>
  } else {
    return (
      <a href={ props.text } target='_blank' rel='noopener noreferrer'>
        <p className={ `help ${hiddenStyle} ${props.style}` }>{ text }</p>
      </a>
    )
  }
}

ToolTip.propTypes = {
  text: PropTypes.string,
  style: PropTypes.string,
  clickable: PropTypes.bool,
}


export default Form
