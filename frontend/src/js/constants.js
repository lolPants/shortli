const constants = {
  ERR_CANNOT_SHORTEN: 'You cannot shorten that URL',
  ERR_UNKNOWN: 'Something went wrong...',
  ERR_URL_MISSING: 'URL Not Specified',
  ERR_URL_INVALID: 'Invalid URL',
  ERR_API_UNREACHABLE: 'API is unreachable',
  ERR_OFFLINE: 'You are offline (check your network)',

  BUTTON_SHORTEN: 'Shorten',
  INPUT_PLACEHOLDER: 'URL',

  DEFAULT_TITLE: 'Shortli',
  DEFAULT_SUBTITLE: 'A simple, deployable URL shortening service built on Node.js, Redis and Docker.',
}

export default constants
