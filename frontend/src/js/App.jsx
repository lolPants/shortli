import React, { Component } from 'react'
import Form from './Form.jsx'

import c from './constants'

import '../css/main.css'
import noise from '../images/noise.png'

/**
 * @typedef {Object} Meta
 * @property {string} title
 * @property {string} subtitle
 */

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      title: localStorage.getItem('title') || '',
      subtitle: localStorage.getItem('subtitle') || '',
    }

    this.fetchMeta()
  }

  async fetchMeta () {
    let meta = await fetch('/api/v1.0/meta')
    let d = {
      title: c.DEFAULT_TITLE,
      subtitle: c.DEFAULT_SUBTITLE,
    }

    if (meta.status === 200) {
      try {
        /**
         * @type {Meta}
         */
        let json = await meta.json()
        this.saveMeta(json, true)
      } catch (err) {
        this.saveMeta(d)
      }
    } else {
      this.saveMeta(d)
    }
  }

  /**
   * @param {Meta} json Metadata in JSON format
   * @param {boolean} [force] Whether to force a save
   */
  saveMeta (json, force = false) {
    this.setState(json)

    if (!localStorage.getItem('title') || force) {
      localStorage.setItem('title', json.title)
      localStorage.setItem('subtitle', json.subtitle)
    }
  }

  render () {
    return (
      <section className='hero is-dark is-fullheight' style={{
        backgroundImage: `url(${noise})`,
        backgroundColor: 'hsl(0, 0%, 11%)',
        overflow: 'hidden',
      }}>
        <div className='hero-body'>
          <div className='container has-text-centered'>
            <h1 className='title'>
              { this.state.title }
              <a href='https://gitlab.com/lolPants/shortli' target='_blank' rel='noopener noreferrer'>
                <svg className='gitlab-icon' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>
                  <path
                    fill='currentColor'
                    d='M29.782 199.732L256 493.714 8.074 309.699c-6.856-5.142-9.712-13.996-7.141-21.993l28.849-87.974zm75.405-174.806c-3.142-8.854-15.709-8.854-18.851 0L29.782 199.732h131.961L105.187 24.926zm56.556 174.806L256 493.714l94.257-293.982H161.743zm349.324 87.974l-28.849-87.974L256 493.714l247.926-184.015c6.855-5.142 9.711-13.996 7.141-21.993zm-85.404-262.78c-3.142-8.854-15.709-8.854-18.851 0l-56.555 174.806h131.961L425.663 24.926z' // eslint-disable-line
                  />
                </svg>
              </a>
            </h1>
            <h2 className='subtitle'>
              { this.state.subtitle }
            </h2>

            <Form />
          </div>
        </div>

        <div className='hero-foot'>
          <div className='foot has-text-centered'>
            <p>
              <strong>Shortli</strong> by <a href='https://www.jackbaron.com' target='_blank' rel='noopener noreferrer'>Jack Baron</a>.
            </p>
          </div>
        </div>
      </section>
    )
  }
}

export default App
