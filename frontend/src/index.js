import React from 'react'
import ReactDOM from 'react-dom'
import App from './js/App.jsx'
import registerServiceWorker from './js/worker/registerServiceWorker'

import 'bulma/css/bulma.css'

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()
